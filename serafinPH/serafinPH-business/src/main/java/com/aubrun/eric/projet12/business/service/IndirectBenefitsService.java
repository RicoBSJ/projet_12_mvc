package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.IndirectBenefitsConsumer;
import com.aubrun.eric.projet12.model.IndirectsBenefits;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IndirectBenefitsService {

    private final IndirectBenefitsConsumer indirectBenefitsConsumer;
    private final JwtToken jwtToken;

    public IndirectBenefitsService(IndirectBenefitsConsumer indirectBenefitsConsumer, JwtToken jwtToken) {
        this.indirectBenefitsConsumer = indirectBenefitsConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<IndirectsBenefits> getAllIndirectBenefits() {
        return indirectBenefitsConsumer.findAllIndirectBenefits(jwtToken);
    }
}
