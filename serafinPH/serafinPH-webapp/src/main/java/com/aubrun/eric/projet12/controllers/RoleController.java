package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.business.service.RoleService;
import com.aubrun.eric.projet12.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class RoleController {

    private final RoleService roleService;
    private final NomenclatureUserService nomenclatureUserService;

    public RoleController(RoleService roleService, NomenclatureUserService nomenclatureUserService) {
        this.roleService = roleService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @ModelAttribute(value = "role")
    public Role setRole(){return new Role();}

    @GetMapping(value = {"/roles", "/getAllRoles"})
    public String printAllRoles(ModelMap modelMap) {
        Roles role = roleService.getAllRoles().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("role", role);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }
}
