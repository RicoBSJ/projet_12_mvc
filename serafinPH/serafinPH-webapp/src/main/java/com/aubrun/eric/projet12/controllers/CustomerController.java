package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.*;
import com.aubrun.eric.projet12.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class CustomerController {

    private final CustomerService customerService;
    private final NomenclatureUserService nomenclatureUserService;
    private final JuridicProtectionService juridicProtectionService;
    private final EstablishmentService establishmentService;
    private final NeedsService needsService;
    private final DirectBenefitsService directBenefitsService;
    private final IndirectBenefitsService indirectBenefitsService;

    public CustomerController(CustomerService customerService, NomenclatureUserService nomenclatureUserService, JuridicProtectionService juridicProtectionService, EstablishmentService establishmentService, NeedsService needsService, DirectBenefitsService directBenefitsService, IndirectBenefitsService indirectBenefitsService) {
        this.customerService = customerService;
        this.nomenclatureUserService = nomenclatureUserService;
        this.juridicProtectionService = juridicProtectionService;
        this.establishmentService = establishmentService;
        this.needsService = needsService;
        this.directBenefitsService = directBenefitsService;
        this.indirectBenefitsService = indirectBenefitsService;
    }

    @ModelAttribute(value = "customer")
    public AddCustomer setAddCustomer() {
        return new AddCustomer();
    }

    @ModelAttribute(value = "nomenclatureUser")
    public NomenclatureUser setNomenclatureUser() {
        return new NomenclatureUser();
    }

    @ModelAttribute(value = "juridicProtection")
    public JuridicProtection setJuridicProtection() {
        return new JuridicProtection();
    }

    @ModelAttribute(value = "establishment")
    public Establishment setEstablishment(){
        return new Establishment();
    }

    @ModelAttribute(value = "needs")
    public Needs setNeeds(){
        return new Needs();
    }

    @ModelAttribute(value = "directBenefits")
    public DirectBenefits setDirectBenefits(){
        return new DirectBenefits();
    }

    @ModelAttribute(value = "indirectBenefits")
    public IndirectBenefits setIndirectBenefits(){
        return new IndirectBenefits();
    }

    @GetMapping("/addCustomer")
    public String showNewCustomer(ModelMap modelMap) {
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("customer", new AddCustomer());
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "../view/addCustomer";
    }

    @GetMapping("/addNomenclatureUser/{customerId}")
    public String showNomenclatureUser(ModelMap modelMap, @PathVariable("customerId") int customerId){
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addNomenclatureUser";
    }

    @GetMapping("/addReleaseDate/{customerId}")
    public String showCurrentCustomer(ModelMap modelMap, @PathVariable("customerId") int customerId, Principal principal) {
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addReleaseDate";
    }

    @GetMapping("/addJuridicProtection/{customerId}")
    public String showJuridicProtection(ModelMap modelMap, @PathVariable("customerId") int customerId){
        JuridicProtections juridicProtections = juridicProtectionService.getAllJuridicProtection().getBody();
        modelMap.addAttribute("juridicProtections", juridicProtections);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addJuridicProtection";
    }

    @GetMapping("/addEstablishmentCustomer/{customerId}")
    public String showEstablishment(ModelMap modelMap, @PathVariable("customerId") int customerId){
        Establishments establishments = establishmentService.getAllEstablishments().getBody();
        modelMap.addAttribute("establishments", establishments);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addEstablishmentCustomer";
    }

    @GetMapping("/addNeeds/{customerId}")
    public String showNeeds(ModelMap modelMap, @PathVariable("customerId") int customerId){
        Need need = needsService.getAllNeeds().getBody();
        modelMap.addAttribute("need", need);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addNeeds";
    }

    @GetMapping("/addDirectBenefits/{customerId}")
    public String showDirectBenefits(ModelMap modelMap, @PathVariable("customerId") int customerId){
        DirectsBenefits directsBenefits = directBenefitsService.getAllDirectBenefits().getBody();
        modelMap.addAttribute("directsBenefits", directsBenefits);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addDirectBenefits";
    }

    @GetMapping("/addIndirectBenefits/{customerId}")
    public String showIndirectBenefits(ModelMap modelMap, @PathVariable("customerId") int customerId){
        IndirectsBenefits indirectsBenefits = indirectBenefitsService.getAllIndirectBenefits().getBody();
        modelMap.addAttribute("indirectsBenefits", indirectsBenefits);
        modelMap.addAttribute("customerId", customerId);
        return "../view/addIndirectBenefits";
    }

    @GetMapping(value = {"/customer", "/getAllCustomer"})
    public String printAllCustomers(ModelMap modelMap, Principal principal) {
        Customers customers = customerService.findCustomers(principal).getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("customers", customers);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }

    @GetMapping(value = {"/detailsCustomer/{customerId}"})
    public String detailsCustomer(ModelMap modelMap, @PathVariable("customerId") int customerId) {
        Customer customer = customerService.findCustomerById(customerId).getBody();
        modelMap.addAttribute("customer", customer);
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "../view/detailsCustomer";
    }

    @PostMapping(value = "/registrationCustomer")
    public ModelAndView customer(@ModelAttribute("customer") AddCustomer customer, AddNomenclatureUser addNomenclatureUser){
        ResponseEntity<Void> addCustomer = customerService.addCustomer(customer, addNomenclatureUser.getNomenclatureUserId());
        System.out.println("Prénom : " + customer.getCustomerFirstName());
        System.out.println("Nom : " + customer.getCustomerLastName());
        System.out.println("Date de naissance : " + customer.getDateBirth());
        System.out.println("Numéro de sécurité sociale : " + customer.getSocialSecurityNumber());
        System.out.println("Mutuelle : " + customer.getMutualName());
        System.out.println("Date d'entrée : " + customer.getEntryDate());
        System.out.println("Âge : " + customer.getAge());
        System.out.println("Référent : " + addNomenclatureUser.getNomenclatureUserId());
        System.out.println("IdCustom : " + customer.getCustomerId());
        ModelAndView modelAndView = new ModelAndView("../view/addCustomerSuccess");
        modelAndView.addObject("message", "Usager créé :");
        modelAndView.addObject("customerLastName", customer.getCustomerLastName());
        modelAndView.addObject(addCustomer);
        return modelAndView;
    }

    @PostMapping(value = "/addNomenclatureUser")
    public String addNomenclatureUser(@ModelAttribute("addNomenclatureUser") AddNomenclatureUser addNomenclatureUser, int customerId, ModelMap modelMap, Principal principal){
        customerService.addNomenclatureUser(addNomenclatureUser.getNomenclatureUserId(), customerId);
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "customer";
    }

    @PostMapping(value = "/addNeeds")
    public String addNeeds(@ModelAttribute("addNeeds") AddNeeds addNeeds , ModelMap modelMap, Principal principal){
        customerService.addNeeds(addNeeds.getCustomerId(), addNeeds.getNeedsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        Need need = needsService.getAllNeeds().getBody();
        modelMap.addAttribute("need", need);
        return "customer";
    }

    @PostMapping(value = "/addDirectBenefits")
    public String addDirectBenefits(@ModelAttribute("addDirectBenefits") AddDirectBenefits addDirectBenefits, ModelMap modelMap, Principal principal){
        customerService.addDirectBenefits(addDirectBenefits.getCustomerId(), addDirectBenefits.getDirectBenefitsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        DirectsBenefits directsBenefits = directBenefitsService.getAllDirectBenefits().getBody();
        modelMap.addAttribute("directsBenefits", directsBenefits);
        return "customer";
    }

    @PostMapping(value = "/addIndirectBenefits")
    public String addIndirectDirectBenefits(@ModelAttribute("addIndirectBenefits") AddIndirectBenefits addIndirectBenefits, ModelMap modelMap, Principal principal){
        customerService.addIndirectBenefits(addIndirectBenefits.getCustomerId(), addIndirectBenefits.getIndirectBenefitsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        IndirectsBenefits indirectsBenefits = indirectBenefitsService.getAllIndirectBenefits().getBody();
        modelMap.addAttribute("indirectsBenefits", indirectsBenefits);
        return "customer";
    }

    @PostMapping(value = "/addJuridicProtection")
    public String addJuridicProtectionCustomer(@ModelAttribute("addJuridicProtection") AddJuridicProtection addJuridicProtection, ModelMap modelMap, Principal principal){
        customerService.updateJuridicProtection(addJuridicProtection.getCustomerId(), addJuridicProtection.getJuridicProtectionId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        JuridicProtections juridicProtections = juridicProtectionService.getAllJuridicProtection().getBody();
        modelMap.addAttribute("juridicProtections", juridicProtections);
        return "customer";
    }

    @PostMapping(value = "/addEstablishmentCustomer")
    public String addEstablishment(@ModelAttribute("addEstablishmentCustomer") AddEstablishmentCustomer addEstablishmentCustomer, ModelMap modelMap, Principal principal) {
        customerService.updateEstablishment(addEstablishmentCustomer.getCustomerId(), addEstablishmentCustomer.getEstablishmentId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        Establishments establishments = establishmentService.getAllEstablishments().getBody();
        modelMap.addAttribute("establishments", establishments);
        return "customer";
    }

    @PostMapping(value = "/addReleaseDate")
    public String updateCustomer(@ModelAttribute("addReleaseDate") AddReleaseDate addReleaseDate, ModelMap modelMap, Principal principal){
        customerService.updateCustomer(addReleaseDate.getCustomerId(), addReleaseDate.getReleaseDate());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }

    @PostMapping(value = "/delNeed")
    public String delNeed(@ModelAttribute("deleteNeedsForm") DeleteNeedsForm deleteNeedsForm, ModelMap modelMap, Principal principal){
        customerService.deleteNeeds(deleteNeedsForm.getCustomerNeedsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }

    @PostMapping(value = "/delDirectBenefits")
    public String delDirectBenefits(@ModelAttribute("deleteDirectBenefitsForm") DeleteDirectBenefitsForm deleteDirectBenefitsForm, ModelMap modelMap, Principal principal){
        customerService.deleteDirectBenefits(deleteDirectBenefitsForm.getCustomerDirectBenefitsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }

    @PostMapping(value = "/delIndirectBenefits")
    public String delIndirectNeeds(@ModelAttribute("deleteIndirectBenefitsForm") DeleteIndirectBenefitsForm deleteIndirectBenefitsForm, ModelMap modelMap, Principal principal){
        customerService.deleteIndirectNeeds(deleteIndirectBenefitsForm.getCustomerIndirectBenefitsId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }

    @PostMapping(value = "/deleteJuridicProtection")
    public String delJuridic(@ModelAttribute("deleteJuridicProtectionForm") DeleteJuridicProtectionForm deleteJuridicProtectionForm, ModelMap modelMap, Principal principal) {
        customerService.deleteJuridicProtection(deleteJuridicProtectionForm.getCustomerJuridicProtectionId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }

    @PostMapping(value = "/deleteEstablishment")
    public String delEstablishment(@ModelAttribute("deleteEstablishmentForm") DeleteEstablishmentForm deleteEstablishmentForm, ModelMap modelMap, Principal principal) {
        customerService.deleteEstablishment(deleteEstablishmentForm.getCustomerEstablishmentId());
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "customer";
    }
}
