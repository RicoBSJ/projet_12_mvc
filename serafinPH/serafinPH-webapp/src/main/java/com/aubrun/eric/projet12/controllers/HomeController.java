package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.CustomerService;
import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.SearchCustomer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class HomeController {

    private final CustomerService customerService;

    public HomeController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(value = {"", "/","/home", "/homePage"})
    public ModelAndView home() {
        SearchCustomer searchCustomer = new SearchCustomer();
        List<Customer> result = customerService.searchCustomer(searchCustomer).getBody();
        ModelAndView modelAndView = new ModelAndView("home", "searchCustomer", new SearchCustomer());
        modelAndView.addObject("customers", result);
        return modelAndView;
    }
}