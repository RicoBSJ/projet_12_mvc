package com.aubrun.eric.projet12.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity(debug = true)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(customAuthenticationProvider);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // http builder configurations for authorize requests and form login (see below)
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/signUpForm").hasAuthority("ROLE_ADMIN")
                .antMatchers("/registration").hasAuthority("ROLE_ADMIN")
                .antMatchers("/getAllCustomerModerator").hasAuthority("ROLE_MODERATOR")
                .antMatchers("/loginProcess").permitAll()
                .antMatchers("/signInForm").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/signInForm")
                /*.loginProcessingUrl("/loginProcess")*/
                .defaultSuccessUrl("/", true)
                .and()
                .logout()
                .logoutUrl("/deconnect")
                .deleteCookies("JSESSIONID");
    }
}