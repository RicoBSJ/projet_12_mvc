<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 27/12/2020
  Time: 00:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Inscription</title>
</head>
<body>
<div class="container bg-light p-5">
    <form>
        <h1 class="p-1 text-uppercase bg-success">Réussite de la création d'utilisateur</h1>
        <p class="p-5">L'utilisateur a correctement été créé.</p>
        <div class="form-group row">
            <div class="col-sm-6">
                <br/>
                <table class="table table-striped table-bordered">
                    <tr>
                        <td><b>Name </b>: ${lastName}</td>
                    </tr>
                </table>
                <br/>
                <p>
                    <a> ${lastName} est bien inscrit</a>
                </p>
                <br/>
                <p>
                    <a class="col-sm-6 p-3 text-center bg-light" href="<c:url value="signInForm" />">Vous pouvez vous connecter ici</a>
                <p/>
                <br/>
                <p>
                    <a class="col-sm-6 p-3 text-center bg-light" href="<c:url value="/"/>">Retour à l'accueil</a>
                </p>
                <br/>
                <br/>
            </div>
        </div>
        <br/>
    </form>
</div>
</body>
</html>
