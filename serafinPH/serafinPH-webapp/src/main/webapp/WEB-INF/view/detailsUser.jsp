<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 04/09/2022
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Page de détails d'un utilisateur</title>
</head>
<body>
<div class="container bg-light p-5">
    <form modelattribute="nomenclatureUser" method="post" action="detailsUser/${nomenclatureUser.nomenclatureUserId}">
        <h1 class="p-1 text-uppercase bg-danger">Page de détails de ${nomenclatureUser.lastName}</h1>
        <p class="p-5">Vous pouvez accéder aux détails d'un utilisateur via cette page.</p>
        <p>
            <a href="<c:url value="/"/>">Retour à l'accueil</a>
        </p>
        <p>
            <a href="<c:url value="/customer"/>">Retour à l'espace utilisateur</a>
        </p>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th scope="col">NOM</th>
                    <th scope="col">EMAIL</th>
                    <th scope="col">LISTE D'USAGERS</th>
                    <th scope="col">RÔLE(S)</th>
                    <th scope="col">ETABLISSEMENT(S)</th>
                </tr>
                </thead>
                <tbody>
                <tr class="align-middle text-center">
                    <td>${nomenclatureUser.lastName}</td>
                    <td>${nomenclatureUser.email}</td>
                    <td>
                        <c:forEach var="customer" items="${nomenclatureUser.customerList}">
                            <p>${customer.customerLastName} ${customer.customerFirstName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="role" items="${nomenclatureUser.roles}">
                            <p>${role.roleName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="establishment" items="${nomenclatureUser.establishments}">
                            <p>${establishment.establishmentName}</p>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <form:hidden path="nomenclatureUser.nomenclatureUserId" value="1"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>