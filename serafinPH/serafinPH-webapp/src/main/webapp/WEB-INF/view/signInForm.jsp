<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 24/12/2020
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Page de connexion</title>
</head>
<body>
<div class="container bg-light p-5">
    <form method="post" action="${pageContext.request.contextPath}/loginProcess">
        <h1 class="p-1 text-uppercase bg-warning">Formulaire de connexion</h1>
        <p class="p-5">Vous pouvez vous connecter via ce formulaire.</p>
        <div class="form-group row">
            <div class="col-sm-6 text-center">
                <label for="username">Email<span class="requis">*</span></label>
                <input type="email" id="username" name="username" placeholder="Entrez votre email"/>
                <br/>
            </div>
            <div class="col-sm-6 text-center">
                <label for="password">Mot de passe<span class="requis">*</span></label>
                <input type="password" id="password" name="password" placeholder="Entrez votre mot de passe"/>
                <br/>
            </div>
        </div>
        <br/>
        <input type="submit" value="Connexion">
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>
