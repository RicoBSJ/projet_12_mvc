package com.aubrun.eric.projet12.model;

public class DeleteRoleForm {

    private Integer nomenclatureUserRoleId;

    public Integer getNomenclatureUserRoleId() {
        return nomenclatureUserRoleId;
    }

    public void setNomenclatureUserRoleId(Integer nomenclatureUserRoleId) {
        this.nomenclatureUserRoleId = nomenclatureUserRoleId;
    }
}
