package com.aubrun.eric.projet12.model;

public class JuridicProtection {

    private Integer juridicProtectionId;
    private EJuridicProtection juridicProtectionName;

    public JuridicProtection() {
    }

    public Integer getJuridicProtectionId() {
        return juridicProtectionId;
    }

    public void setJuridicProtectionId(Integer juridicProtectionId) {
        this.juridicProtectionId = juridicProtectionId;
    }

    public EJuridicProtection getJuridicProtectionName() {
        return juridicProtectionName;
    }

    public void setJuridicProtectionName(EJuridicProtection juridicProtectionName) {
        this.juridicProtectionName = juridicProtectionName;
    }
}
