package com.aubrun.eric.projet12.model;

public class IndirectBenefits {

    private Integer indirectBenefitsId;
    private String indirectBenefitName;
    private EIndirectBenefits indirectBenefitType;

    public IndirectBenefits() {
    }

    public Integer getIndirectBenefitsId() {
        return indirectBenefitsId;
    }

    public void setIndirectBenefitsId(Integer indirectBenefitsId) {
        this.indirectBenefitsId = indirectBenefitsId;
    }

    public String getIndirectBenefitName() {
        return indirectBenefitName;
    }

    public void setIndirectBenefitName(String indirectBenefitName) {
        this.indirectBenefitName = indirectBenefitName;
    }

    public EIndirectBenefits getIndirectBenefitType() {
        return indirectBenefitType;
    }

    public void setIndirectBenefitType(EIndirectBenefits indirectBenefitType) {
        this.indirectBenefitType = indirectBenefitType;
    }
}
