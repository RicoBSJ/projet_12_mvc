package com.aubrun.eric.projet12.model;

import java.util.List;
import java.util.Set;

public class AddCustomer {

    private Integer customerId;
    private NomenclatureUser nomenclatureUser;
    private String customerFirstName;
    private String customerLastName;
    private String dateBirth;
    private Long socialSecurityNumber;
    private String mutualName;
    private String entryDate;
    private String releaseDate;
    private Integer age;
    private Set<JuridicProtection> juridicProtectionList;
    private Set<Establishment> establishmentList;
    private List<Needs> needsList;
    private List<DirectBenefits> directBenefitsList;
    private List<IndirectBenefits> indirectBenefitsList;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public NomenclatureUser getNomenclatureUser() {
        return nomenclatureUser;
    }

    public void setNomenclatureUser(NomenclatureUser nomenclatureUser) {
        this.nomenclatureUser = nomenclatureUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<JuridicProtection> getJuridicProtectionList() {
        return juridicProtectionList;
    }

    public void setJuridicProtectionList(Set<JuridicProtection> juridicProtectionList) {
        this.juridicProtectionList = juridicProtectionList;
    }

    public Set<Establishment> getEstablishmentList() {
        return establishmentList;
    }

    public void setEstablishmentList(Set<Establishment> establishmentList) {
        this.establishmentList = establishmentList;
    }

    public List<Needs> getNeedsList() {
        return needsList;
    }

    public void setNeedsList(List<Needs> needsList) {
        this.needsList = needsList;
    }

    public List<DirectBenefits> getDirectBenefitsList() {
        return directBenefitsList;
    }

    public void setDirectBenefitsList(List<DirectBenefits> directBenefitsList) {
        this.directBenefitsList = directBenefitsList;
    }

    public List<IndirectBenefits> getIndirectBenefitsList() {
        return indirectBenefitsList;
    }

    public void setIndirectBenefitsList(List<IndirectBenefits> indirectBenefitsList) {
        this.indirectBenefitsList = indirectBenefitsList;
    }
}
