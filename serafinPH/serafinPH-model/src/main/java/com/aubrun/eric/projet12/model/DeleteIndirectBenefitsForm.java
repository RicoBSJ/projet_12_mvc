package com.aubrun.eric.projet12.model;

public class DeleteIndirectBenefitsForm {

    private Integer customerIndirectBenefitsId;

    public Integer getCustomerIndirectBenefitsId() {
        return customerIndirectBenefitsId;
    }

    public void setCustomerIndirectBenefitsId(Integer customerIndirectBenefitsId) {
        this.customerIndirectBenefitsId = customerIndirectBenefitsId;
    }
}
