package com.aubrun.eric.projet12.model;

public enum ENeeds {

    HEALTH_NEEDS,
    AUTONOMY_NEEDS,
    SOCIAL_PARTICIPATION_NEEDS
}
