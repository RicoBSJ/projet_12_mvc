package com.aubrun.eric.projet12.model;

public class Needs {

    private Integer needsId;
    private String needName;
    private ENeeds needType;

    public Needs() {
    }

    public Integer getNeedsId() {
        return needsId;
    }

    public void setNeedsId(Integer needsId) {
        this.needsId = needsId;
    }

    public String getNeedName() {
        return needName;
    }

    public void setNeedName(String needName) {
        this.needName = needName;
    }

    public ENeeds getNeedType() {
        return needType;
    }

    public void setNeedType(ENeeds needType) {
        this.needType = needType;
    }
}
