package com.aubrun.eric.projet12.model;

public class DeleteDirectBenefitsForm {

    private Integer customerDirectBenefitsId;

    public Integer getCustomerDirectBenefitsId() {
        return customerDirectBenefitsId;
    }

    public void setCustomerDirectBenefitsId(Integer customerDirectBenefitsId) {
        this.customerDirectBenefitsId = customerDirectBenefitsId;
    }
}
