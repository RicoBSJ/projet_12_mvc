package com.aubrun.eric.projet12.model;

public class DeleteJuridicProtectionForm {

    private Integer customerJuridicProtectionId;

    public Integer getCustomerJuridicProtectionId() {
        return customerJuridicProtectionId;
    }

    public void setCustomerJuridicProtectionId(Integer customerJuridicProtectionId) {
        this.customerJuridicProtectionId = customerJuridicProtectionId;
    }
}
