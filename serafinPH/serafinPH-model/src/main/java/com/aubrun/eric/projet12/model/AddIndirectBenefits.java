package com.aubrun.eric.projet12.model;

public class AddIndirectBenefits {

    private Integer indirectBenefitsId;
    private Integer customerId;

    public Integer getIndirectBenefitsId() {
        return indirectBenefitsId;
    }

    public void setIndirectBenefitsId(Integer indirectBenefitsId) {
        this.indirectBenefitsId = indirectBenefitsId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
