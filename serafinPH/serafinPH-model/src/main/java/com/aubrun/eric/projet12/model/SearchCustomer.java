package com.aubrun.eric.projet12.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SearchCustomer {

    private Integer customerId;
    private String nomenclatureUser;
    private String customerFirstName;
    private String customerLastName;
    private LocalDate dateBirth;
    private Long socialSecurityNumber;
    private String mutualName;
    private LocalDateTime entryDate;
    private Integer age;

    public SearchCustomer() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getNomenclatureUser() {
        return nomenclatureUser;
    }

    public void setNomenclatureUser(String nomenclatureUser) {
        this.nomenclatureUser = nomenclatureUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
