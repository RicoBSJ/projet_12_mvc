package com.aubrun.eric.projet12.model;

import java.util.List;
import java.util.Set;

public class AddNomenclatureUser {

    private Integer nomenclatureUserId;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Integer phoneUser;
    private String dateBirthUser;
    private Long socialSecurityNumberUser;
    private String entryDateUser;
    private String releaseDateUser;
    private Integer ageUser;
    private List<Customer> customerList;
    private Set<Role> roles;
    private Set<Establishment> establishments;

    public Integer getNomenclatureUserId() {
        return nomenclatureUserId;
    }

    public void setNomenclatureUserId(Integer nomenclatureUserId) {
        this.nomenclatureUserId = nomenclatureUserId;
    }

    public String getUsername() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(Integer phoneUser) {
        this.phoneUser = phoneUser;
    }

    public String getDateBirthUser() {
        return dateBirthUser;
    }

    public void setDateBirthUser(String dateBirthUser) {
        this.dateBirthUser = dateBirthUser;
    }

    public Long getSocialSecurityNumberUser() {
        return socialSecurityNumberUser;
    }

    public void setSocialSecurityNumberUser(Long socialSecurityNumberUser) {
        this.socialSecurityNumberUser = socialSecurityNumberUser;
    }

    public String getEntryDateUser() {
        return entryDateUser;
    }

    public void setEntryDateUser(String entryDateUser) {
        this.entryDateUser = entryDateUser;
    }

    public String getReleaseDateUser() {
        return releaseDateUser;
    }

    public void setReleaseDateUser(String releaseDateUser) {
        this.releaseDateUser = releaseDateUser;
    }

    public Integer getAgeUser() {
        return ageUser;
    }

    public void setAgeUser(Integer ageUser) {
        this.ageUser = ageUser;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Establishment> getEstablishments() {
        return establishments;
    }

    public void setEstablishments(Set<Establishment> establishments) {
        this.establishments = establishments;
    }
}
